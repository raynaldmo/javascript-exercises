// Miscellaneous code snippets for testing
delete o;
delete arr;
delete obj;
// var o = {
//    x:1, y:2, arr: ['apple', 'orange'], obj: {a:100, b:200},
//    toString: function() {console.info(this.x, this.y, this.arr.toString(), this.obj.toString())}
// };

var o = new Object({ x:1, y:2, arr: ['apple', 'orange'], obj: {a:100, b:200},
    toString: function() {console.info(this.x, this.y, this.arr.toString(), this.obj.toString())} });
    
// console.info(Object.getOwnPropertyNames(o));
// console.info(o.toString());
// console.info(Object.getPrototypeOf(o));

var F = function func (a, b) {};
console.info('1', Object.getOwnPropertyNames(F));

var arr = Object.getOwnPropertyNames(F);

for (var i = 0; i < arr.length; i++) {
    console.info(typeof arr[i], arr[i]);
    console.log(F[arr[i]]);
}

console.log('+++++++++++++++++++++++++++++');

console.info('2', Object.prototype);
console.info('2.5', Object.prototype.constructor);
console.info('3', Object.keys(F));

for (var p in F) {
    console.info('4', p);
}


var p = F.prototype;
console.info('4.5', p);
console.info('5', p.constructor);

console.info('6', Object.getOwnPropertyNames(Array));
console.info('7', Array.prototype);
console.info('8', Array.prototype.constructor);

var arr = Object.getOwnPropertyNames(F);

for (var i = 0; i < arr.length; i++ ){
    var val = F[arr[i]];
    console.info(arr[i], val, 'typeof = ', typeof val);
    
    if (val == null) continue;
    if (typeof val == 'object' || typeof val == 'function') {
        console.info('\t', arr[i], Object.getOwnPropertyNames(val));
        
    } else {
        console.info('\t', arr[i], 'Not object or function');
    }
}

console.info(F.prototype.constructor);
console.info(Object.prototype.constructor);
console.info(Array.prototype.constructor);

function inheritedPropertyNames(obj) {
    if ( (typeof obj) !== "object") {
        throw new Error("Only objects are allowed");
    }
    
    var props = {};
    
    while(obj) {
        // for each property, dynamically create a new property
        // in props object!
        Object.getOwnPropertyNames(obj).forEach(function(p) {
            props[p] = true;
        });
        obj = Object.getPrototypeOf(obj);
    }
    return Object.getOwnPropertyNames(props);
}

inheritedPropertyNames( {foo: "abc"} );

