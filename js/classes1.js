// classes1.js
// This version does not use constructors

// Load functions on document load
// Need click handler
// Process input (numbers and radio selection)
// Display output

window.onload = function() {
    'use strict';
    function range(from, to) {
        // Create object r
        var r = Object.create(range.methods);
        // Add properties 'from' and 'to' to r
        r.from = from;
        r.to = to;
        return r;
    }
    // Create object called range.methods
    range.methods = {
        // includes: function(x) { console.info('x = ', x); return this.from <= x && x <= this.to; },
        includes: function(x) {
            console.info('x = ', x, 'typeof x = ', typeof x);
            if (this.from <= x && x <= this.to) {
                return true;
            } 
            return false;
        },
        foreach: function(f) {
            for (var x = Math.ceil(this.from); x<=this.to; x++) f(x);
        },
        toString: function() { return "(" + this.from + "..." + this.to + ")"; }
    };

    document.getElementById('button').onclick = function() {

        // get the input numbers
        var input = document.getElementById('input');
        if (!input) {console.info('Could not get input element!'); return false; } 

        // convert string into array
        input = (input.value).split(' ');

        // create our object
        // convert to number to prevent string to number comparisons!!
        var r = range(parseInt(input[0], 10), parseInt(input[1], 10));
        console.info(r.from, r.to);

        var output = '';

        console.info(input[0], input[1], input[2]);

        // see which radio button is pressed
        var radios = document.getElementsByTagName('input');
        for (var i = 0; i < radios.length; i++) {
            if (radios[i].type === 'radio' && radios[i].checked) {
                switch (radios[i].value) {
                case 'includes':
                    output = r.includes(parseInt(input[2], 10));
                    break;
                case 'tostring':
                    output = r.toString();
                    break;
                default:
                    output = 'Unknown operation';
                    break;
                }
            }
        }                    
        // display output
        document.getElementById('output').value = output;

        return true;
    };
};
