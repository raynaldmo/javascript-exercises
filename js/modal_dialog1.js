// Return the viewport size as w and h properties of an object
function getViewportSize(w) {
    // Use the specified window or the current window if no argument
    w = w || window;

    // This works for all browsers except IE8 and before
    if (w.innerWidth != null) return {w: w.innerWidth, h:w.innerHeight};

    // For IE (or any browser) in Standards mode
    var d = w.document;
    if (document.compatMode == "CSS1Compat") {
        return {w: d.documentElement.clientWidth, h: d.documentElement.clientHeight};
    }
    // For browsers in Quirks mode
    return {w: d.body.clientWidth, h:d.body.clientHeight};
}

function processClick() {
    'use strict';

    if (window.screen.width && window.screen.height) {
        // For debug only, print how big the screen is
        console.log('screenWidth=', window.screen.width,
            ' screenHeight=', window.screen.height);
    }
    
    // get window size
    var obj = getViewportSize();
    var width = obj.w, height = obj.h;
    
    console.log('windowWidth=', width, ' windowHeight=', height);

    // get window position
    var xpos, ypos;
    if (window.screenX) {
        xpos = window.screenX;
    } else if (window.screenLeft) {
        // for IE
        xpos = window.screenLeft;
    }

    if (window.screenY) {
        ypos = window.screenY;
    } else if (window.screenTop) {
        // for IE
        ypos = window.screenTop;
    }
 
    console.log ('xPos=', xpos, 'yPos=', ypos);
   
    // 11/1/13: Strange thing: if main window left edge is at the left edge of
    // the screen, xpos gets returned as undefined
    if (xpos === undefined || ypos === undefined) {
        return false;
    }

    // make new window a little smaller than current window
    width *= 0.66;
    height *= 0.66;

    // roughtly center new windows
    ypos += width/4;
    xpos += height/2;
        
    var windowPos = "dialogwidth:" + width + "; dialogheight:" + 
                     height + "; dialogtop:" +
                     ypos + "; dialogleft:" + xpos + "; resizable:yes; status:no";

    // Open dialog window
    window.showModalDialog("../examples-test/multiprompt.html",
            ["Enter 3D point coordinates", "x", "y", "z"], windowPos);

    return false; // prevent default action on link click
}

window.onload = function() {

// Define browser.name and browser.version for client sniffing, using code
// derived from jQuery 1.4.1. Both the name and number are strings, and both
// may differ from the public browser name and version. Detected names are:
//
//   "webkit": Safari or Chrome; version is WebKit build number
//   "opera": the Opera browser; version is the public version number
//   "mozilla": Firefox or other gecko-based browsers; version is Gecko version
//   "msie": IE; version is public version number
//
// Firefox 3.6, for example, returns: { name: "mozilla", version: "1.9.2" }.
    var browser = (function() {
        var s = navigator.userAgent.toLowerCase();
        var match = /(webkit)[ \/]([\w.]+)/.exec(s) ||
	    /(opera)(?:.*version)?[ \/]([\w.]+)/.exec(s) ||
	    /(msie) ([\w.]+)/.exec(s) ||
	    !/compatible/.test(s) && /(mozilla)(?:.*? rv:([\w.]+))?/.exec(s) ||
	    [];
     return { name: match[1] || "", version: match[2] || "0" };
    }());

    console.log(browser.name + ' ' + browser.version);

    // showModalDiaglog seems to work reliably for all browsers except IE
    if (browser.name != 'msie') {
        document.getElementById('anchor').onclick = processClick;
    }
};



