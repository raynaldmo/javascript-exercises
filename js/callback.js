/**
 * Created by raynald on 1/11/14.
 */

// Trying to understand how callbacks work!

// 1. First take example of object invoking a method.

var obj = { x:1, f: function f(x) { return this.x = x } };

// 2.f() takes single parameters, evaluates it and returns result
// An important point is that f() must return a value, otherwise res
// will be undefined!

var res = obj.f(1000);
console.info(100, res); // 1000

// 3. Now for callback example
Array.prototype.test = function (f) {
    console.info(200, this);
    return f(this);
}

var arr = [1, 2, 3];

var res = arr.test(function(a) {  // anonymous function used as callback (f())
    return a;
});

console.info(400,res);

// 4. When test() is invoked, _this_ value is arr since test was invoked
// as a method call arr.test()

// 5. test() is passed a function (callback). This is reflected in
// the Array.prototype.test declaration

// 6. test() invokes f() and returns a value

// 7. value from is returned as res

// 8. IMPORTANT! if callback f() did not return a value, res would end up being
// undefined!

// 9. In essence, callback f() introduces a level of indirection. Instead of
// test() returning a result, test() invokes f() which then returns a result!


// Another example
// Implement my own version of forEach
Array.prototype.myForEach = function (f) {
    console.info(500, this);
    // for each array value, invoke callback
    for (var i = 0;i < this.length; i++) {
        f(this[i], i);
    }
}

// this works too!
Array.prototype.myForEach1 = function () {
    console.info(500, this);
    // for each array value, invoke callback
    for (var i = 0;i < this.length; i++) {
        arguments[0](this[i], i);  // <====== LOOKY HERE!!
    }
}

var names = ['Mike', 'Stacy', 'Rick'];
names.myForEach(function(name, idx){
    console.info(600, idx + 1 + "." + name);
 });

names.myForEach1(function(name, idx){
    console.info(600, idx + 1 + "." + name);
});