// Display a message in a special debugging output section of 
// document.
// If the document does not contain such a section, create one.
function debug(msg) {
    'use strict';

    // Find the debugging section of the document, looking at HTML
    // id attributes
    var log = document.getElementById("debuglog");

    // If no element with the id "debuglog" exists, create one.
    if (!log) {
        log = document.createElement("div"); // Create a new <div> element
        log.id = "debuglog";                 // Set the HTML id attribute on it
        log.innerHTML = "<h3>Debug Log</h3>"; // Define initial content
        document.body.appendChild(log);       // Add it at end of document
    } 

    // Now wrap the message in its own <pre> and append it to the log
    var pre = document.createElement("pre");    // Creat the <pre> tag
    var text = document.createTextNode(msg);    // Wrap msg in a text node  
    pre.appendChild(text);                      // Add text to <pre>
    log.appendChild(pre);                       // Add <pre> to the log
}

function logMsg() {
    'use strict';
    var msg = document.getElementById('input').value;
    debug(msg);
}

window.onload = function() {
    'use strict';
    document.getElementById('button').onclick = logMsg;
};
