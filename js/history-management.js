/**
 * Created by raynald on 1/20/14.
 */

// Start a new game when we load
window.onload = newgame;

// Handle history events
// Generated when Back and Forward buttons are clicked!
window.onpopstate = popState;

// Globals initialized in newgame()
var state ,ui;

function newgame(playagain) {
    // Set up an object to hold document elements we care about
    ui = {
        heading: null,
        prompt: null,
        input: null,
        low: null,
        mid: null,
        high: null
    };

    // Look up each of these element ids
    for (var id in ui) {
        if (ui.hasOwnProperty(id)) {
            ui[id] = document.getElementById(id);
        }
    }

    // Define an event handler for the input field
    ui.input.onchange = handleGuess;

    // Pick a random number and initialize game state
    state = {
        n: Math.floor(99 * Math.random()) + 1,
        low: 0,
        high: 100,
        guessnum: 0,
        guess: undefined
    };

    // Modify document to display this initial state
    display(state);

    if (playagain === true) save(state);

} //newgame

// Save game state into browser history with pushState(), if it is supported
function save(state) {
    if(!history.pushState) return;

    // Associate a URL with the saved state
    var url = "#guess" + state.guessnum;
    // Now  save the state object and the URL
    history.pushState(state,  // State object to save
                        "", // State title: current browsers ignore this
                        url); // State URL
} // save

function popState(event) {
    // If the event has a state object, restore that state.
    // Note that event.state is a deep copy of the saved state object
    // se we can modify it without altering the saved value
    console.info('popState: ' + event.state);
    if (event.state) {
        state = event.state;
        display(state);
    } else {
        // When we load the page for the first time, we'll get a popstate event
        // with no state. Replace that null state with our real state
        // state is a global!
        history.replaceState(state, "", "#guess" + state.guessnum);
    }
}

// This event handler is invoked each time the user guesses a number
// It updates the game state, saves it, and displays it.
function handleGuess() {
    //Get the user's guess from the input field
    var g = parseInt(this.value);
    if (isNaN(g)) {
        alert("You didn't enter a number. Please enter a number between 0 and 100")
        return;
    }
    if ( (g > state.low) && g < state.high) {
        // Update the state object based on this guess
        if (g < state.n) state.low = g;
        else if (g > state.n) state.high = g;
        state.guess = g;
        state.guessnum++;

        // Save state in browser's history
        save(state);

        // Update document to respond to user's guess
        display(state);
    } else {
        // An invalid guess: don't push a new history state
        alert("Please enter a number greater than " + state.low +
            " and less than " + state.high);
    }
} // handleGuess

// Modify the document to display the current state of the game
function display(state) {
    // Display document heading and title
    ui.heading.innerHTML = document.title =
        "I'm thinking of a number between " + state.low +
        " and " + state.high + ".";

    // Display a visual representation of the range of numbers using a table
    ui.low.style.width = state.low + "%";
    ui.mid.style.width = (state.high - state.low) + '%';
    ui.high.style.width = (100 - state.high) + '%';

    // Make sure the input field is visible, empty and focused
    ui.input.style.visibility = 'visible';
    ui.input.value = "";
    ui.input.focus();

    // Set the prompt based on the user's most recent guess
    if (state.guess === undefined)
        ui.prompt.innerHTML = 'Type your guess and hit Enter: ';
    else if (state.guess < state.n)
        ui.prompt.innerHTML = state.guess +  " is too low. Guess again: ";
    else if (state.guess > state.n)
        ui.prompt.innerHTML = state.guess + " is too high. Guess again: ";
    else {
        // When correct, hide the input field and show a Play Again button
        ui.input.style.visibility = "hidden";
        ui.heading.innerHTML = document.title = state.guess + " is correct! ";
        ui.prompt.innerHTML =
            "You Win! <button onclick='newgame(true)'>Play Again</button>";
    }
}