// fill in table  
       $(document).ready(function() {
            // build table header
            var th1 = '<th>Operation</th>';
            var th2 = '<th>$(target).method(content)</th>';
            var th3 = '<th>$(content).method(target)</th>';
            
            $('#content').append('<table id="table" border="1"' + '<tr>'+
                th1 + th2 + th3 + '</tr>' + '</table>');

            // build first row data
            var td1 = '<td>Insert content at end of target</td>';
            var td2 = '<td>append()</td>';
            var td3 = '<td>appendTo()</td>';
            // add to table
            $('<tr>' + td1 + td2 + td3 + '</tr>').appendTo('#table');

            // build second row data
            td1 = '<td>Insert content at start of target</td>';
            td2 = '<td>prepend()</td>';
            td3 = '<td>prependTo()</td>';
            // add to table
            $('<tr>' + td1 + td2 + td3 + '</tr>').appendTo('#table');

            // build third row data
            td1 = '<td>Insert content after target</td>';
            td2 = '<td>after()</td>';
            td3 = '<td>insertAfter()</td>';
            // add to table
            $('<tr>' + td1 + td2 + td3 + '</tr>').appendTo('#table');

            // build fourth row data
            td1 = '<td>Insert content before target</td>';
            td2 = '<td>before()</td>';
            td3 = '<td>insertBefore()</td>';
            // add to table
            $('<tr>' + td1 + td2 + td3 + '</tr>').appendTo('#table');

            // build fifth row data
            td1 = '<td>Replace target with content</td>';
            td2 = '<td>replaceWith()</td>';
            td3 = '<td>replaceAll()</td>';
            // add to table
            $('<tr>' + td1 + td2 + td3 + '</tr>').appendTo('#table');

        }); // document ready 

