function onLoad(f) {
    'use strict';
    if (onLoad.loaded)
        window.setTimeout(f, 0);
    else if (window.addEventListener)
        window.addEventListener("load", f, false);
    else if (window.attachEvent)
        // for IE         
        window.attachEvent("onload", f);
}

onLoad.loaded = false;

onLoad(function() { // Anonymous function defines a local scope
    'use strict';

    // Find the TOC contained element.
    // If there isn't one create one at the start of the document.
    var toc = document.getElementById("TOC");
    if (!toc) {
        toc = document.createElement("div");
        toc.id = "TOC";
        console.log(document.body.firstChild.nodeName);
        document.body.insertBefore(toc, document.body.firstChild);
    }

    // Find all section heading elements
    var headings;
    if (document.querySelectorAll) { // Can we do it the easy way?
        headings = document.querySelectorAll("h1, h2, h3, h4, h5, h6");
    } else {
        console.log('No querySelector method');
        return;
    }

    // Initialize an array that keeps track of section numbers.
    var sectionNumbers =  [0,0,0,0,0,0];

    // Now loop through the section header elements we found.
    for (var h = 0; h < headings.length; h++) {
        var heading = headings[h];

        // Skip the section heading if it is inside the TOC container.
        if (heading.parentNode == toc) continue;

        // Figure out what level heading it is
        var level = parseInt(heading.tagName.charAt(1), 10);
        if (isNaN(level) || level < 1 || level > 6) continue;

        // Increment the section number for this heading level and
        // reset all lower heading level numbers to zero.
        sectionNumbers[level-1]++;
        for (var i = level; i < 6; i++) sectionNumbers[i] = 0;

        // Now combine section numbers for all heading levels to produce
        // a section number like 2.3.1
        var sectionNumber = sectionNumbers.slice(0,level).join(".");
        console.log('h'+ level + ' : ' + sectionNumber);

        // Add the section number to the section header title.
        // We place the number in a <span> to make it stylable.
        var span = document.createElement("span");
        span.className = "TOCSectNum";
        span.innerHTML = sectionNumber;

        console.log('h'+ level + ' : ' + heading.firstChild.nodeValue);
        heading.insertBefore(span, heading.firstChild);

        // Wrap the heading in a named anchor so we can link to it.
        var anchor = document.createElement("a");
        anchor.name = "TOC"+sectionNumber;
        heading.parentNode.insertBefore(anchor, heading);
        anchor.appendChild(heading);

        // Now create a link to this section.
        var link = document.createElement("a");
        link.href = "#TOC" + sectionNumber;
        link.innerHTML = heading.innerHTML; // Link text is same as heading

        // Place the link in a div that is styleable based on the level.
        var entry = document.createElement("div");
        entry.className = "TOCEntry TOCLevel" + level;
        entry.appendChild(link);

        // And add the div to the TOC container.
        toc.appendChild(entry);
    }
});
