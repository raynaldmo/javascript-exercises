(function() {

    function showToolTip(e) {
        e = e || window.event;
        var target = e.target || e.srcElement;

        console.log('node name= ', target.nodeName, ' class= ', 
            target.className);

        // find out where the image is
        var box = target.getBoundingClientRect();
        console.log('x = ' + box.left + ' y = ' + box.top);

        // get tooltip 
        // var selector = '.bio_highlight_tooltip .' + target.className;
        // var selector = 'ul.' + target.className;        
        var selector = 'bio_highlight_tooltip ' + target.className;  
        console.log(selector);

        //var tooltip = document.querySelector(selector);
        var elems = document.getElementsByClassName(selector);    
        console.info('Found ' + elems.length + ' tooltip');
        var tooltip = elems[0];

        if (tooltip) {
            console.log('node name= ', tooltip.nodeName, 'class= ',
                tooltip.className);

            console.log('tooltip x= ' + tooltip.style.left +
                ' tooltip y= ' + tooltip.style.top);

            // position tooltip next to image
            tooltip.style.left = '105px';
            tooltip.style.top = '-151px';
            // show it
            tooltip.style.display = 'block';
        }
    
    } // showToolTip()

    function hideToolTip(e) {
        e = e || window.event;
        var target = e.target || e.srcElement;
        console.log('node name= ', target.nodeName, ' class= ', 
            target.className);

        var selector = 'bio_highlight_tooltip ' + target.className;  
        console.log(selector);

        var elems = document.getElementsByClassName(selector);    
        console.info('Found ' + elems.length + ' tooltip');
        var tooltip = elems[0];

        if (tooltip) {
            console.log('node name= ', tooltip.nodeName, 'class= ',
                tooltip.className);

            console.log('tooltip x= ' + tooltip.style.left +
                ' tooltip y= ' + tooltip.style.top);
            // hide it
            tooltip.style.display = 'none';
            tooltip.style.left = '0px';
            tooltip.style.top = '0px';
        }
    }

    // execution starts here
    // alg
    // get all images
    // install mouseover and mouseout event for each

    // hide all ul elements
    var ul = document.getElementsByTagName('ul')[0];
    ul.style.display = 'none';

    var images = document.images;
    for (var i = 0, cnt = images.length; i < cnt; i++) {
        images[i].onmouseover = showToolTip;
        images[i].onmouseout = hideToolTip;
    }

})();
