// Quiz application
// Start quiz when user clicks button
window.onload = function() {
 
   document.getElementById('start').onclick = function() {

        // The Question constructor is the parent for all Question objects
        function Question(theQuestionNum, theQuestion, theChoices, theCorrectAnswer) {
            // initialize instance properties
            this.questionNum = theQuestionNum;
            this.question = theQuestion;
            this.choices = theChoices;
            this.correctAnswer = theCorrectAnswer;
            this.userAnswer = "";

            // private properties; these cannot be changed by instances
            var newDate = new Date();

            // Constant variable: availabel to all instances through the
            // instance method below.
            var QUIZ_CREATED_DATE = newDate.toLocaleDateString();

            // This is the only way to access the private QUIZ_CREATED_DATE
            // variable.
            // This is an example of a privelege method: it can access private 
            // properties and it can be called publicly
            this.getQuizDate = function () {
                return QUIZ_CREATED_DATE;
            };
    
            // Confirmation message that question was created
            console.info("Question: " + this.question + " created.");

        } // Question constructor

        // Define prototype methods that will be inherited
        // Define them this like this also prevents duplicating these methods
        // in each instance
        Question.prototype.getCorrectAnswer = function () {
            return this.correctAnswer;
        };

        Question.prototype.getUserAnswer = function() {
            return this.userAnswer;
        };

        // Display the question in radio button list format.
        // This method can be overriden to display the questions in an
        // alternative format
        Question.prototype.displayQuestion = function() {
            
           console.info('displayQuestion:' + this.questionNum);
 
           var questionToDisplay = 
                this.questionNum + ". " + this.question + "<ul>";

                var choiceCounter = 0;

//            this.choices.forEach(function(eachChoice) {
//                    questionToDisplay += '<li><input type="radio" name="choice' + 1 '"' + 'value="'  + choiceCounter + '">' + eachChoice + '</li>';
//                    choiceCounter++;
//                });

            for (var i = 0, cnt = this.choices.length; i < cnt; i++) {
                questionToDisplay += '<li><input type="radio" name="choice' + 
                this.questionNum + '"' + 'value="'  + choiceCounter + '">' + 
                this.choices[i] + '</li>';
                choiceCounter++;                
            }

            questionToDisplay += "</ul>";
            console.info(questionToDisplay);

            return questionToDisplay;

        };

        function MultipleChoiceQuestion(theQuestionNum, theQuestion, theChoices, 
            theCorrectAnswer) {
            
            // This adds Question type properties to
            // MultipleChoice instances.
            Question.call(this, theQuestionNum, theQuestion, theChoices, theCorrectAnswer);            
            // same as this.Question(theQuestion, theChoices, theCorrectAnswer);
            // but we cannot use the above call since instances of
            // MultipleChoice do not have a Question method. 
            // So we "force" it to have it by using the 'call' method
            // slick!

        } // MutipleChoiceQuestion constructor

        function inheritPrototype(childObject, parentObject) {
            var copyOfParent = Object.create(parentObject.prototype);
            if(1) {
                copyOfParent.constructor = childObject;
                childObject.prototype = copyOfParent;
            } else {
                // Alternate way
                childObject.prototype = copyOfParent;
                childObject.prototype.constructor = childObject;
            }
        }

        // execution starts here
        if (typeof Object.create !== 'function') {
           console.info("Object.create function doesn't exist.."); 
           Object.create = function (o) {
                function F() {}
                F.prototype = o;
                return new F();
            };
        } else {
            console.info("Object.create function exists...");
        }

        // main starts here
        // Inherit Question methods
        inheritPrototype(MultipleChoiceQuestion, Question);       

        // Build questions
        // allQuestions is an array. Each element of the array is a
        // MultipleChoice object
        var allQuestions = [
            new MultipleChoiceQuestion(1, "What is the capital of California?",
                ["Sacramento", "Los Angeles", "San Francisco"], 0),

            new MultipleChoiceQuestion(2,  "What is the capital of Illinois?",
                ["Chicago", "Rockford", "Springfield"], 2)
        ];

        // Display the questions
//        allQuestions.forEach(function(eachQuestion) {
//            eachQuestion.displayQuestion();
//        });

        // get where output should go
        var content = document.getElementById('content');
        // output.innerHTML = "<div class='question'>";
        // output.innerHTML = '';        

        for (var i = 0, cnt = allQuestions.length; i < cnt; i++) {
            var div = document.createElement('div');
            div.className = 'question';
            div.innerHTML = allQuestions[i].displayQuestion();
            content.appendChild(div);

//           output.innerHTML += allQuestions[i].displayQuestion();               
        }
//        output.innerHTML += "</div>";

        document.getElementById('submit').onclick = function() {
            // for each question get checked radio button
            // compare checked button with correct answer
            // output 'Correct' (green) or 'Correct answer is: xxx' (red)

            // Get users answers to the questions            
            var answer = []; // keep user answers here
            var correctAnswers = 0; // running count of correct answers
            // get all questions - identified by 'question' class
            var question = content.getElementsByClassName('question');
 
            // for each question
            for (var i = 0, cnt = question.length; i < cnt; i++)  {                        
                // get all radio buttons for this question
                console.info('i = ' + i + ' cnt = ' + cnt);
                var radio = question[i].getElementsByTagName('input');

                for (var j = 0, cnt1 = radio.length; j < cnt1; j++) {
                    if (radio[j].type === 'radio' && radio[j].checked) {
                        answer[i] = radio[j].value;
                        console.info(answer[i]);
                        break;
                    }
                }
                // check answers
                var correctAnswer = allQuestions[i].getCorrectAnswer();
                var p = document.createElement('p');                   
                if (correctAnswer == parseInt(answer[i], 10)) {
                    p.style.color = 'green';
                    p.innerHTML = 'Correct';
                    question[i].appendChild(p);
                    correctAnswers++;
                } else {
                    p.style.color = 'red';
                    p.innerHTML = 'Correct answer is ' + 
                    allQuestions[i].choices[correctAnswer];
                        question[i].appendChild(p);
                }
            } // each quesion

            // post results
            var results = document.getElementById('results');

            results.innerHTML = '<br>You answered ' + correctAnswers + '/' +
                allQuestions.length + ' questions correctly';
 
        }; // submit click handler

    };// start click handler

}; // onload

