function f(a, b) {
    return a + b;
}

function trace(o, m) {
    var original = o[m];
    o[m] = function () {
        console.log((new Date()).toString(), "Entering:", m);
        var result = original.apply(this, arguments);
        console.log((new Date()).toString(), "Exiting:", m);
        return result;
    };
}


var o = {x:1, y:2, mult: function () { return this.x * this.y;} };


o.x = 10;
o.y = 5;

trace(o, 'mult');

console.log(o.mult());

