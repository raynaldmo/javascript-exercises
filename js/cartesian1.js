// cartesian1.js

// function to calculate polar coordinates of x, y

function calculate(obj) {
    'use strict';
    var result = [];
    // result.push(obj.r.toFixed(2));
    // result.push(obj.theta.toFixed(2));
    result.push( [obj.r.toFixed(2), obj.theta.toFixed(2)] );    
    return result;   
}

function doOper() {
    'use strict';

// Define object literal with setter and getter functions
var p = {
    x: 1.0,
    y: 1.0,
    // r is a read-write accessor property with getter and setter.
    // Don't forget to put a comma after accessor methods.
    get r() { return Math.sqrt(this.x * this.x + this.y * this.y); },
    set r(newvalue) {
        var oldvalue = Math.sqrt(this.x * this.x + this.y * this.y);
        var ratio = newvalue/oldvalue;
        this.x *= ratio;
        this.y *= ratio;
    },

    // theta is a read-only accessor property with getter only.
    get theta() { return Math.atan2(this.y, this.x); }
};

    // get values
    var input = document.getElementById('input').value;
    console.log('doOper: input = ' + input);

    var values = input.split(' ');
    p.x = values[0];
    p.y = values[1];
    console.log('x = ' + p.x + ' y = ' + p.y);

    values = calculate(p);

    document.getElementById('output').value = values[0][0] + ' ' + values[0][1];
}

// load the event handler
window.onload = function () {
    'use strict';
    document.getElementById('button').onclick = doOper;
};
