// Event properties
// On any browser that supports addEventListener() method, event
// handlers receive a single argument, which represents the event that
// occurred.
function handler(e) {
    e = e || window.event; // window.event is for IE8 and earlier
    var target = e.target || e.srcElement; // srcElement for IE8 and earlier

    // target is HTML element that triggered the event
    ......
}
