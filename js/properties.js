// properties.js
// script to display properties of objects in table
// format


window.onload = function() {
    'use strict';
 
    var objects = ["Object", "Object.prototype"];
   
    // add rows to table
    var table = document.getElementById("myTable");

    for (var i = 0 , cnt = objects.length; i < cnt; i++) {
        var row = table.insertRow(-1);        
        var o = objects[i];    
        row.insertCell(0).innerHTML = o;
        row.insertCell(1).innerHTML = typeof o;
        row.insertCell(2).innerHTML = eval(o + '.constructor');
        // row.insertCell(3).innerHTML = eval(o + '.getPrototypeOf' + '(' + o + ')');
        row.insertCell(3).innerHTML = eval(o + '.getOwnPropertyNames' + '(' + o + ')');
        row.insertCell(4).innerHTML = eval(o + '.keys' + '(' + o + ')');        
    }
};
