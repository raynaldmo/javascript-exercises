// classes3.js
// This version uses the defineClass function from chapter 9

// Load functions on document load
// Need click handler
// Process input (numbers and radio selection)
// Display output

window.onload = function() {
    'use strict';

    function extend(o, p) {
        for (var prop in p) {  // for all properties in p
            o[prop] = p[prop]; // Add the property to o
        }
    }

    function defineClass(constructor, methods, statics) {
        if (methods) extend(constructor.prototype, methods);
        if (statics) extend(constructor, statics);
        return constructor;
    }

    // Create the SimpleRange class
    var SimpleRange = 
        defineClass(function(f,t) { this.from = f; this.to = t; }, // constructor
            {
                includes: function(x) { return this.from <= x && x <= this.to; },
                toString: function() { return this.from + "..." + this.to; }
            }, // methods
            {
                upto: function(t) { return new SimpleRange(0, t); }
            } // statics
        );

    document.getElementById('button').onclick = function() {
        // get the input numbers
        var input = document.getElementById('input');
        if (!input) {console.info('Could not get input element!'); return false; } 

        // convert string into array
        input = (input.value).split(' ');

        // create our object
        // convert to number to prevent string to number comparisons!!
        var r = new SimpleRange(parseInt(input[0], 10), parseInt(input[1], 10));
        console.info(r.from, r.to);

        var output = '';

        console.info(input[0], input[1], input[2]);

        // see which radio button is pressed
        var radios = document.getElementsByTagName('input');
        for (var i = 0; i < radios.length; i++) {
            if (radios[i].type === 'radio' && radios[i].checked) {
                switch (radios[i].value) {
                    case 'includes':
                        output = r.includes(parseInt(input[2], 10));
                        break;
                    case 'tostring':
                        output = r.toString();
                        break;
                    default:
                        output = 'Unknown operation';
                    break;
                }
            }
        }                    
        // display output
        document.getElementById('output').value = output;

        return true;
    };
};
