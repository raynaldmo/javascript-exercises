/**
 * Created by raynald on 1/7/14.
 */
/* Implement alternate version of Array.map and Array.sort */

(function() {
    Array.prototype.mymap = function (f) {
        var arr = [];
        var len = this.length;
        for (var i = 0; i < len; i++) {
            // var n = f(this[i]);
            arr.push( f( this[i] ) );
        }
        return arr;
    };


    var names = ['Fred', 'Wilma', 'Pebbles'];

    var upper = names.mymap(function(name) {
        return name.toUpperCase();
    });

    console.info(upper);

    // use bubble sort
    Array.prototype.mysort = function (f) {
        var len = this.length, n = 0;
        for (var i = 0; i < len - 1; i++) {
            for (var j = i+1; j < len; j++) {
                n++;
                console.log('n:' + n + ' i:' + i + ' j:' + j);
                if ( f( this[i], this[j]) > 0 ) {
                    var temp = this[i];
                    this[i] = this[j];
                    this[j] = temp;
                }
            }
        }
    }

    var arr = [3, 9, 12, 2, 1 ];

    arr.mysort(function(x, y) {
        if (x < y) return -1;
        if (x > y) return 1;
        return 0;
    });

    console.info(arr);


    // Another example
    // Implement my own version of forEach
    // arr.myForEach(function(i, idx, arr) {
    //    console_log(idx, i);
    // });
    Array.prototype.myForEach = function (f) {
        // for each array value, invoke callback
        for (var i = 0;i < this.length; i++) {
            f(this[i], i);
        }
    };

    // this works too!
    Array.prototype.myForEach1 = function () {
        console.info(500, this);
        // for each array value, invoke callback
        for (var i = 0;i < this.length; i++) {
            arguments[0](this[i], i);  // <====== LOOKY HERE!!
        }
    }
})();


