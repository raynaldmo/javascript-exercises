// properties.js
// script to display properties of objects in table
// format


window.onload = function() {
    'use strict';
    
    // instantiate dummy objects in order to examine them
    function F () {};
    F.prototype.n = 100;
    F.prototype.s = 'fruit';
    F.prototype.a = [45, 20, 3];
    F.prototype.o = {x:1, y:2, z:'hello'};
    F.prototype.f = function () { this.a.sort(); }


    function Person(name, age, job) { // constructor
        this.name = name;
        this.age = age;
        this.job = job;
        this.friends = ["Mike", "Player"];
    }

    Person.prototype = {
        constructor: Person,
        sayName : function() {
            alert(this.name);
        }
    };

    var person = new Person("Raynald Mompoint", 53, "Software Engineer");

    var O = {x:1, s:'apple'};
    var A = [1, 2, 3];
    var S = new String("abcd");
    var N = new Number(1001);
    var D = new Date("July 2, 1960");
    var R = new RegExp('d(b+)d', 'gi');

    var objects = [ 
                    ['Function', 'typeof Function', classof(Function),
                     'Function.constructor',
                     'Object.getPrototypeOf(Function)', 
                     'Object.getOwnPropertyNames(Function).sort()', 
                     'Object.keys(Object).sort()',
                     '(function () { var arr = [], i = 0;for (var p in Function) {arr[i++] = p;} return arr; })()'
                    ],

                    ['Function.prototype', 'typeof Function.prototype', classof(Function.prototype),
                     'Function.prototype.constructor',
                     'Object.getPrototypeOf(Function.prototype)', 
                     'Object.getOwnPropertyNames(Function.prototype).sort()', 
                     'Object.keys(Function.prototype).sort()',
                     '(function () { var arr = [], i = 0;for (var p in Function.prototype) {arr[i++] = p;}return arr; })()'                    
                   ],

                    ['F', 'typeof F', classof(F), 
                     'F.constructor',
                     'Object.getPrototypeOf(F)', 
                     'Object.getOwnPropertyNames(F).sort()', 
                     'Object.keys(F).sort()',
                     '(function () { var arr = [], i = 0;for (var p in F) {arr[i++] = p;}return arr; })()'                    
                   ],

                    ['F.prototype', 'typeof F.prototype', classof(F.prototype), 
                     'F.prototype.constructor',
                     'Object.getPrototypeOf(F.prototype)', 
                     'Object.getOwnPropertyNames(F.prototype).sort()', 
                     'Object.keys(F.prototype).sort()',
                     '(function () { var arr = [], i = 0;for (var p in F.prototype) {arr[i++] = p;}return arr; })()'                    
                   ],

                    ['Person', 'typeof Person', classof(Person),
                     'Person.constructor',
                     'Object.getPrototypeOf(Person)', 
                     'Object.getOwnPropertyNames(Person).sort()', 
                     'Object.keys(Person).sort()',
                     '(function () { var arr = [], i = 0;for (var p in Person) {arr[i++] = p;}return arr; })()'                    

                    ],

                    ['Person.prototype', 'typeof Person.prototype', classof(Person.prototype), 
                     'Person.prototype.constructor',
                     'Object.getPrototypeOf(Person.prototype)', 
                     'Object.getOwnPropertyNames(Person.prototype).sort()', 
                     'Object.keys(Person.prototype).sort()',
                     '(function () { var arr = [], i = 0;for (var p in Person.prototype) {arr[i++] = p;}return arr; })()'                    

                    ],

                    ['person', 'typeof person', classof(person),
                     'person.constructor',
                     'Object.getPrototypeOf(person)', 
                     'Object.getOwnPropertyNames(person).sort()', 
                     'Object.keys(person).sort()',
                     '(function () { var arr = [], i = 0;for (var p in person) {arr[i++] = p;}return arr; })()'                    

                    ],

                    ['Object', 'typeof Object', classof(Object),
                     'Object.constructor',
                     'Object.getPrototypeOf(Object)', 
                     'Object.getOwnPropertyNames(Object).sort()', 
                     'Object.keys(Object).sort()',
                     '(function () { var arr = [], i = 0;for (var p in Object) {arr[i++] = p;} return arr; })()'
                    ],
                    ['Object.prototype', 'typeof Object.prototype', classof(Object.prototype), 
                     'Object.prototype.constructor',
                     'Object.getPrototypeOf(Object.prototype)', 
                     'Object.getOwnPropertyNames(Object.prototype).sort()', 
                     'Object.keys(Object.prototype).sort()',
                     '(function () { var arr = [], i = 0;for (var p in Object.prototype) {arr[i++] = p;}return arr; })()'                    
                   ],
                    ['O', 'typeof O', classof(O),
                     'O.constructor',
                     'Object.getPrototypeOf(O)', 
                     'Object.getOwnPropertyNames(O).sort()', 
                     'Object.keys(O).sort()',
                     '(function () { var arr = [], i = 0;for (var p in O) {arr[i++] = p;}return arr; })()'                    
                   ],
                    ['Array', 'typeof Array', classof(Array),
                     'Array.constructor',
                     'Object.getPrototypeOf(Array)', 
                     'Object.getOwnPropertyNames(Array).sort()', 
                     'Object.keys(Array).sort()',
                     '(function () { var arr = [], i = 0;for (var p in Array) {arr[i++] = p;} return arr; })()'
                    ],
                    ['Array.prototype', 'typeof Array.prototype', classof(Array.prototype), 
                     'Array.prototype.constructor',
                     'Object.getPrototypeOf(Array.prototype)', 
                     'Object.getOwnPropertyNames(Array.prototype).sort()', 
                     'Object.keys(Array.prototype).sort()',
                     '(function () { var arr = [], i = 0;for (var p in Array.prototype) {arr[i++] = p;} return arr; })()'
                    ],
                    ['A', 'typeof A', classof(A),
                     'A.constructor',
                     'Object.getPrototypeOf(A)', 
                     'Object.getOwnPropertyNames(A).sort()', 
                     'Object.keys(A).sort()',
                     '(function () { var arr = [], i = 0;for (var p in A) {arr[i++] = p;} return arr; })()'
                    ],
                    ['String', 'typeof String', classof(String),
                     'String.constructor',
                     'Object.getPrototypeOf(String)', 
                     'Object.getOwnPropertyNames(String).sort()', 
                     'Object.keys(String).sort()',
                     '(function () { var arr = [], i = 0;for (var p in String) {arr[i++] = p;} return arr; })()'
                    ],

                    ['String.prototype', 'typeof String.prototype', classof(String.prototype), 
                     'String.prototype.constructor',
                     'Object.getPrototypeOf(String.prototype)', 
                     'Object.getOwnPropertyNames(String.prototype).sort()', 
                     'Object.keys(String.prototype).sort()',
                     '(function () { var arr = [], i = 0;for (var p in String.prototype) {arr[i++] = p;} return arr; })()'
                    ],
                    ['S', 'typeof S', classof(S),
                     'S.constructor',
                     'Object.getPrototypeOf(S)', 
                     'Object.getOwnPropertyNames(S).sort()', 
                     'Object.keys(S).sort()',
                     '(function () { var arr = [], i = 0;for (var p in S) {arr[i++] = p;} return arr; })()'
                    ],
                    ['Number', 'typeof Number', classof(Number),
                     'Number.constructor',
                     'Object.getPrototypeOf(Number)', 
                     'Object.getOwnPropertyNames(Number).sort()', 
                     'Object.keys(Number).sort()',
                     '(function () { var arr = [], i = 0;for (var p in Number) {arr[i++] = p;} return arr; })()'
                    ],
                    ['Number.prototype', 'typeof Number.prototype', classof(Number.prototype), 
                     'Number.prototype.constructor',
                     'Object.getPrototypeOf(Number.prototype)', 
                     'Object.getOwnPropertyNames(Number.prototype).sort()', 
                     'Object.keys(Number.prototype).sort()',
                     '(function () { var arr = [], i = 0;for (var p in Number.prototype) {arr[i++] = p;} return arr; })()'
                    ],
                    ['N', 'typeof N', classof(N),
                     'N.constructor',
                     'Object.getPrototypeOf(N)', 
                     'Object.getOwnPropertyNames(N).sort()', 
                     'Object.keys(N).sort()',
                     '(function () { var arr = [], i = 0;for (var p in N) {arr[i++] = p;} return arr; })()'
                    ],
                    ['Date', 'typeof Date', classof(Date),
                     'Date.constructor',
                     'Object.getPrototypeOf(Date)', 
                     'Object.getOwnPropertyNames(Date).sort()', 
                     'Object.keys(Date).sort()',
                     '(function () { var arr = [], i = 0;for (var p in Date) {arr[i++] = p;} return arr; })()'
                    ],
                    ['Date.prototype', 'typeof Date.prototype', classof(Date.prototype), 
                     'Date.prototype.constructor',
                     'Object.getPrototypeOf(Date.prototype)', 
                     'Object.getOwnPropertyNames(Date.prototype).sort()', 
                     'Object.keys(Date.prototype).sort()',
                     '(function () { var arr = [], i = 0;for (var p in Date.prototype) {arr[i++] = p;} return arr; })()'
                    ],
                    ['D', 'typeof D', classof(D),
                     'D.constructor',
                     'Object.getPrototypeOf(D)', 
                     'Object.getOwnPropertyNames(D).sort()', 
                     'Object.keys(D).sort()',
                     '(function () { var arr = [], i = 0;for (var p in D) {arr[i++] = p;} return arr; })()'
                    ],

                    ['RegExp', 'typeof RegExp', classof(RegExp),
                     'RegExp.constructor',
                     'Object.getPrototypeOf(RegExp)', 
                     'Object.getOwnPropertyNames(RegExp).sort()', 
                     'Object.keys(RegExp).sort()',
                     '(function () { var arr = [], i = 0;for (var p in RegExp) {arr[i++] = p;} return arr; })()'
                    ],
                    ['RegExp.prototype', 'typeof RegExp.prototype', classof(RegExp.prototype), 
                     'RegExp.prototype.constructor',
                     'Object.getPrototypeOf(RegExp.prototype)', 
                     'Object.getOwnPropertyNames(RegExp.prototype).sort()', 
                     'Object.keys(RegExp.prototype).sort()',
                     '(function () { var arr = [], i = 0;for (var p in RegExp.prototype) {arr[i++] = p;} return arr; })()'
                    ],
                    ['R', 'typeof R', classof(R),
                     'R.constructor',
                     'Object.getPrototypeOf(R)', 
                     'Object.getOwnPropertyNames(R).sort()', 
                     'Object.keys(R).sort()',
                     '(function () { var arr = [], i = 0;for (var p in R) {arr[i++] = p;} return arr; })()'
                    ],

                    ['Node', 'typeof Node', classof(Node),
                     'Node.constructor',
                     '', 
                     'Object.getOwnPropertyNames(Node).sort()', 
                     'Object.keys(Node).sort()',
                     '(function () { var arr = [], i = 0;for (var p in Node) {arr[i++] = p;} return arr; })()'
                    ],
                    ['Node.prototype', 'typeof Node.prototype', classof(Node.prototype), 
                     'Node.prototype.constructor',
                     'Object.getPrototypeOf(Node.prototype)', 
                     'Object.getOwnPropertyNames(Node.prototype).sort()', 
                     'Object.keys(Node.prototype).sort()',
                     '(function () { var arr = [], i = 0;for (var p in Node.prototype) {arr[i++] = p;} return arr; })()'
                    ],

                    ['Document', 'typeof Document', classof(Document),
                     'Document.constructor',
                     '', 
                     'Object.getOwnPropertyNames(Document).sort()', 
                     'Object.keys(Document).sort()',
                     '(function () { var arr = [], i = 0;for (var p in Document) {arr[i++] = p;} return arr; })()'
                    ],
                    ['Document.prototype', 'typeof Document.prototype', classof(Document.prototype), 
                     'Document.prototype.constructor',
                     'Object.getPrototypeOf(Document.prototype)', 
                     'Object.getOwnPropertyNames(Document.prototype).sort()', 
                     'Object.keys(Document.prototype).sort()',
                     '(function () { var arr = [], i = 0;for (var p in Document.prototype) {arr[i++] = p;} return arr; })()'
                    ],

                    ['Element', 'typeof Element', classof(Element), 
                     'Element.constructor',
                     '', 
                     'Object.getOwnPropertyNames(Element).sort()', 
                     'Object.keys(Element).sort()',
                     '(function () { var arr = [], i = 0;for (var p in Element) {arr[i++] = p;} return arr; })()'
                    ],

                    ['Element.prototype', 'typeof Element.prototype', classof(Element.prototype), 
                     'Element.prototype.constructor',
                     'Object.getPrototypeOf(Element.prototype)', 
                     'Object.getOwnPropertyNames(Element.prototype).sort()', 
                     'Object.keys(Element.prototype).sort()',
                     '(function () { var arr = [], i = 0;for (var p in Element.prototype) {arr[i++] = p;} return arr.sort(); })()'
                    ],

                    ['Text', 'typeof Text', classof(Text),
                     'Text.constructor',
                     '', 
                     'Object.getOwnPropertyNames(Text).sort()', 
                     'Object.keys(Text).sort()',
                     '(function () { var arr = [], i = 0;for (var p in Text) {arr[i++] = p;} return arr; })()'
                    ],

                    ['Text.prototype', 'typeof Text.prototype', classof(Text.prototype), 
                     'Text.prototype.constructor',
                     'Object.getPrototypeOf(Text.prototype)', 
                     'Object.getOwnPropertyNames(Text.prototype).sort()', 
                     'Object.keys(Text.prototype).sort()',
                     '(function () { var arr = [], i = 0;for (var p in Text.prototype) {arr[i++] = p;} return arr.sort(); })()'
                    ],
                 ];
   

    // Returns a string with 'class' of object
    function classof(obj) {
        if (obj=== null) return "Null";
        if (obj === undefined) return "Undefined";
        return Object.prototype.toString.call(obj).slice(8, -1);
        // return obj.toString();
    }

    // Prints property names        
    function printPropNames(item, arr) {
        // console.log('printPropNames:item:' + item + ' val: ' + arr + ' typeof val: ' + typeof arr);     
        // if (!arr) { return 'no output'; }
        if (typeof arr == 'undefined') { return 'no output'; }
        if (typeof arr == 'string')  { return arr };
        if ( !(arr instanceof Array) ) { return arr };
        
        // For non arrays return as is   
        // if (arr.constructor !== Array || typeof arr === 'string') { return arr; }

        if(!arr.length) { return 'empty array'; }

        var output = [], j = 0;
        output[j++] = arr[0];
        for (var i = 1; i < arr.length; i++) {
            if (i % 4 == 0) {
                output[j++] = '<br>';
            } 
            output[j++] = arr[i];
        }
        // console.log(output);

        return output;
    }

    // Application starts here!

    // add rows to table
    var table = document.getElementById("myTable");

    for (var type = 0; type < objects.length; type++) {
        var row = table.insertRow(-1);

        for (var item = 0; item < objects[type].length; item++) { 
           // if (objects[type][0] != 'Node') continue;
           // console.log('main: ' + item + ': ' + objects[type][item]);
            if (item == 0 || item == 2) { // these items are known to be strings           
                row.insertCell(item).innerHTML = objects[type][item];
            } else {          
                var res = eval(objects[type][item]);
                if (!res) { 
                    row.insertCell(item).innerHTML = 'undefined result';                           
                } else {
                    row.insertCell(item).innerHTML = printPropNames(item, res);
                }
            }       
        }
    }
};
