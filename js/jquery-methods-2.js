// create table using jQuery  - version #2
$(document).ready(function() {
    'use strict';
    // static array of data to fill table
    // later when I know how, I can fill the array with data read from
    // the network, database or file
    var data = [ 
        ["Insert content at end of target", "append()", "appendTo()"],
        ["Insert content at start of target", "prepend()", "prependTo()"],
        ["Insert content after target", "after()", "insertAfter()"],
        ["Insert content before target", "before()", "insertBefore()"],
        ["Replace target with content", "replaceWith()", "replaceAll()"],
    ];
    
    // create table            
    $('#content').append('<table id="tbl1" border="1"></table>');
    $('#tbl1').append('<thead id="thead1"></thead>');
    $('#tbl1').append('<tbody id="tbod1"></tbody>');
     
    // add headers
    var th1 = '<th>Operation</th>';
    var th2 = '<th>$(target).method(content)</th>';
    var th3 = '<th>$(content).method(target)</th>';
    $('#thead1').append('<tr>'+ th1 + th2 + th3 + '</tr>');
    

    
    // build empty rows
    var s = '';    
    for (var i = 0; i < 5; i++) {
   //     $("#tbod1").append('<tr id="tr' + i + '">' + 
   //         '<td></td><td></td><td></td></tr>');
        s += '<tr id="tr' + i + '">' + '<td></td><td></td><td></td></tr>';    
    }
    
    $("#tbod1").append(s);
    
    // fill in table data
    for (i = 0; i < 5; i++) {
        // select the row
        var row = '#tr' + i;
        for (var j = 1; j < 4; j++) {
            // select the cell
            var cell = ':nth-child(' + j + ')'; 
            // form final selector
            var sel = row + ' > ' + cell;
            console.info(sel);
            $(sel).text(data[i][j - 1]);
        }   
    }
    
    // color the rows
    $('#tbod1 tr:even').css('background-color', 'linen');
    
    
}); // document ready 

