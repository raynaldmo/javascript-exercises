// Sort array of words using bubble sort
// alg
// array = ["Honda", "Mercedes", "Ford", "Chevrolet"];
// 1. point to first entry
// 2. compare to each other entry and swap positions if
// smaller
// 3.point to second entry, repeat steps 1 and 2.

var car = ["Mercedes", "Honda", "Ford", "Audi"];
var n = 0;

console.log(car);

for (var i = 0, cnt = car.length-1; i < cnt ; i++) {
    for (var j = i+1; j < car.length; j++) {
        n++;
        console.log('n:' + n + ' i:' + i + ' j:' + j);
        if ( car[i] > car[j] ) {
            var temp = car[i];
            car[i] = car[j];
            car[j] = temp;
        }
    }
}

console.log(car);
console.log(n);
