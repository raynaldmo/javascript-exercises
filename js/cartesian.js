// cartesian.js

// Define object literal
var p = {
    x: 1.0,
    y: 1.0,
    // r is a read-write accessor property with getter and setter.
    // Don't forget to put a comma after accessor methods.
    get r() { 'use strict'; return Math.sqrt(this.x * this.x + this.y * this.y); },
    set r(newvalue) {
        'use strict';
        var oldvalue = Math.sqrt(this.x * this.x + this.y * this.y);
        var ratio = newvalue/oldvalue;
        this.x *= ratio;
        this.y *= ratio;
    },

    // theta is a read-only accessor property with getter only.
    get theta() { 'use strict'; return Math.atan2(this.y, this.x); }
};

// function to calculate polar coordinates of x, y

function calculate() {
    'use strict';

    // get values
    var input = document.getElementById('input').value;
    console.log('calculate: input = ' + input);

    var values = input.split(' ');
    p.x = values[0];
    p.y = values[1];
    console.log('x = ' + p.x + ' y = ' + p.y);

    document.getElementById('output').value = p.r.toFixed(2) + ' ' + p.theta.toFixed(2);
}

// load the event handler
window.onload = function () {
    'use strict';
    document.getElementById('button').onclick = calculate;
};
