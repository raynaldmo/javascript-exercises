// classes2.js
// This version uses a constructor 

// Load functions on document load
// Need click handler
// Process input (numbers and radio selection)
// Display output

window.onload = function() {
    'use strict';

    function Range(from, to) {
        this.from = from;
        this.to = to;
    }

    // All range objects inherit from this object
    // object literal format
    if(0) {
    Range.prototype = {
        // includes: function(x) { console.info('x = ', x); return this.from <= x && x <= this.to; },
        includes: function(x) {
            console.info('x = ', x, 'typeof x = ', typeof x);
            if (this.from <= x && x <= this.to) {
                return true;
            } 
            return false;
        },
        foreach: function(f) {
            for (var x = Math.ceil(this.from); x<=this.to; x++) f(x);
        },
        toString: function() { return "(" + this.from + "..." + this.to + ")"; }
    };
 
   }
    
    // Use prototype property to add methods
    // similar to add new property to an object
    // i.e. obj.prop = new_property
    if(1) {
        Range.prototype.includes = function(x) { return this.from <= x && x <= this.to; };
        Range.prototype.foreach = function (f) {
            for (var x = Math.ceil(this.from); x <= this.to; x++) f(x);
        };
        Range.prototype.toString = function() {
            return "(" + this.from + "..." + this.to + ")";
        }
    }

    document.getElementById('button').onclick = function() {
        // get the input numbers
        var input = document.getElementById('input');
        if (!input) {console.info('Could not get input element!'); return false; } 

        // convert string into array
        input = (input.value).split(' ');

        // create our object
        // convert to number to prevent string to number comparisons!!
        var r = new Range(parseInt(input[0], 10), parseInt(input[1], 10));
        console.info(r.from, r.to);

        var output = '';

        console.info(input[0], input[1], input[2]);

        // see which radio button is pressed
        var radios = document.getElementsByTagName('input');
        for (var i = 0; i < radios.length; i++) {
            if (radios[i].type === 'radio' && radios[i].checked) {
                switch (radios[i].value) {
                    case 'includes':
                        output = r.includes(parseInt(input[2], 10));
                        break;
                    case 'tostring':
                        output = r.toString();
                        break;
                    default:
                        output = 'Unknown operation';
                    break;
                }
            }
        }                    
        // display output
        document.getElementById('output').value = output;

        return true;
    };
};
