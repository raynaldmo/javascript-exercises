/**
 * Created by raynald on 11/13/14.
 */

var slideShow = {

  $loaded : null,
  num_loaded : 0,
  $img_display : null,
  img_arr : [], // store loaded images here
  next_img_idx : 0, // next image to display
  display_arr : [], // image set to display
  num_display : 4, // number of images to display at one time


  init : function(num, $container) {

    this.num_display = num || this.num_display;

    // create individual 'containers' to display images
    for (var i = 0; i < this.num_display; i++) {
      var $div = $('<div></div>').addClass('image');
      $container.append($div);
    }

    // Now collect all the containers
    this.$img_display = $('.image');
    console.log('display ' + this.$img_display.length + ' images at a time');

    // handlers
    $('#next').click(this.next.bind(this));

    $('#pre').click(this.prev.bind(this));

  },

  next : function() {
    if (this.next_img_idx < this.num_loaded) {
      var img = this.img_arr[this.next_img_idx];
      // shift in next available image
      this.display_arr.unshift(img);
      this.next_img_idx++;
    }
    this.show();
  },

  prev : function() {
    if (this.next_img_idx > this.num_display) {
      // shift images left
      this.display_arr.shift();
      this.next_img_idx--;
    }
    this.show();
  },

  load : function() {
    var i;

    // Find all images on page or images that would be loaded
    // via AJAX for example
    this.$loaded = $('#server').find('img');
    this.num_loaded = this.$loaded.length;
    console.log("loaded " + this.num_loaded + ' images');

    // load images into array
    for (i = 0; i < this.num_loaded; i++) {
      this.img_arr.push(this.$loaded[i]);
    }

    // Load up initial set of images to display
    for (i = 0; i < this.num_display; i++) {
      this.display_arr[i] = this.img_arr[i];
    }

    this.next_img_idx = this.num_display;

  },

  show : function() {
    // Display current image set
    for (var i = 0; i < this.num_display; i++) {
      var img = this.display_arr[i];
      this.$img_display.eq(i).empty();
      this.$img_display.eq(i).append(img);
    }
  },

  print : function() {
    for (var i = 0; i < this.num_loaded; i++) {
      console.log(this.img_arr[i]);
    }
  }

};

slideShow.init(6, $('#media'));
slideShow.load();
slideShow.show();
// slideShow.print();


