console.log(Object.getOwnPropertyNames(Date));
console.log(typeof Date);

// console.log(Object.getOwnPropertyNames(Date.prototype));
console.log(typeof Date.prototype);

var d = new Date();
console.log(d);

console.log(d.getMonth());

Date.prototype.getMonthName = function (m) {
    var months = ["January", "February", "March",
        "April", "May", "June", "July", "August",
        "September", "October", "November",
        "December"];
       return months[m];
     };
     
console.log(d.getMonthName(d.getMonth()));
