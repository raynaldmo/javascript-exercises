var socialMedia = {
  facebook : 'http://facebook.com/viewsource',
  twitter: 'http://twitter.com/planetoftheweb',
  flickr: 'http://flickr.com/planetotheweb',
  youtube: 'http://youtube.com/planetoftheweb'
};


// 1. Find all <nav> tags
// 2. Create <ul> list
// 3. For each entry in socialMedia object, find image that it goes with
// 4. Create <a> element with <img> element


(function() {

  var navs = document.getElementsByClassName('socialmediaicons');

  // console.log(navs.length);

  for (var i= 0; i<navs.length; i++) {
    var ul = document.createElement('ul');

    for (var key in socialMedia) {

      if (socialMedia.hasOwnProperty(key)) {
        // get the link for <a> element
        var link = socialMedia[key];
        var a = document.createElement('a');
        // <li><a href=""><img src='xxx'></a></li>
        a.href = link;

        var img = document.createElement('img');
        img.src = 'images/' + key + '.png';

        a.appendChild(img);

        var li = document.createElement('li');

        // Attach things
        li.appendChild(a);
        ul.appendChild(li);
      }
    }

    // console.dir(ul);
    // console.log(navs[i]);

    navs[i].appendChild(ul);
  }


})();