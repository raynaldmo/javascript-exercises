/**
 * Created by raynald on 3/22/15.
 */

// use strict mode in a module/library
var param = 1000;

var LIB = (function(param) {
  'use strict';

  console.log('param: '+param);

  function init() {
    console.log('init -> '+param);
  }

  function f1() {
    console.log('I am f1()');
  }

  return {init : init, f1 : f1};

})(window.param);


LIB.init();
LIB.f1();