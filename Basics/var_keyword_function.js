/**
 * Created by raynald on 3/22/15.
 */

function F() {
  "use strict";
  // var keyword must be used, otherwise ReferenceError exception
  // when F() is called.
  var msg2 = "I am msg2";
  console.log('F(): '+msg2);
}

F();
// These statements cause a ReferenceError exception, proving that
// that msg2 is not a global variable
// console.log(msg2);
// console.log(window.msg2);

