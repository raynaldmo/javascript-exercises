/**
 * Created by raynald on 3/22/15.
 */
var msg3 = 'I am msg3';

function F1() {
  'use strict';
  console.log('msg3: '+msg3);
  console.log('window.msg3: '+window.msg3);

  // Causes TypeError exception in strict mode
  // console.log('this.msg3: '+this.msg3);
}

F1();