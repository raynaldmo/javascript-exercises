(function ($) {

    // CSVReader constructor
    function CSVReader (separators) {
        this.separators = separators || [","];

        var arr = this.separators.map(function(sep) {
            return  sep;
        });

        var str = arr.join('|');
        this.regexp = new RegExp(str);

        console.info(50, 'arr= ' + arr + ' str= ' + str + ' regxep= ' +
            this.regexp);
    }

    // add read method for all CSVReader instances
    CSVReader.prototype.read = function(str) {
        // create an array - each array entry is a line from str
        var lines = str.trim().split(/\n/);
        console.info(100, lines, lines.length);

        // var self = this;
        // map returns a new array
        // each entry of the array is individual 'token'
        // from lines input
        return lines.map(function (line) {

            // for passed in line array, split line into individual array
            // elements based on regxep
            console.info(150, this.regexp);

            return line.split(this.regexp);

        }, this);
    };

    // captured by closure below!
    function processResponse(response) {

        // console.info('reached processResponse!');

        var reader = new CSVReader([","]);
        var data = reader.read(response);
        // console.info(200, data, data.length);

        // Fill in table! but first clear out any previous data
        $('thead').empty();
        $('tbody').empty();


        // data is an array. Each entry is a sub-array. Each sub-array entry
        // goes into a table cell
        data.forEach(function(arr, idx) {   // main array
            // create new row
            var $tr = $('<tr></tr>');

            arr.forEach(function(val, idx1 ) { // sub-array
                if(idx == 0) {
                    // table header
                    var $th = $('<th></th>').text(val);
                    $tr.append($th);
                    $('thead').append($tr);
                } else {
                    // table body
                    var $td = $('<td></td>').text(val);
                    $tr.append($td);
                    $('tbody').append($tr);
                }
            });
        });
    }

    // captured by closures below!
    var $statusLine = $("#status-line");

    // send ajax request
    function ajaxRequest(url, method, dataType) {
        $.ajax( {
            url: url,
            type: method,
            dataType: dataType,
            cache: false,

            // NOTE: if text I see output if JSON I see no output
            success: function(response, status) {

                $statusLine.html("Request Succeeded! - status " +
                    status).css({color: "green"});

                processResponse(response);
            },

            error: function(xhr, status) {
                $statusLine.html("Request Failed! - status "
                    + status).css({color: "red"});
            },

            complete: function(xhr, status) {
                $('<p></p>').html("Request Completed - status " +
                        status )
                    .css({color: "blue"})
                    .appendTo($statusLine);
            }
        });
    }

    // Install button click handlers
    // simple text file
    $("button").on("click", function() {
        ajaxRequest('./contacts.txt', 'GET', 'text');
    });

})(jQuery);