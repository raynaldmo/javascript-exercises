### About the Files

The .pdf file is the original book.

In the "other-formats" folder, there are MSWord, plain text, and kindle versions
of the book. – Though I should warn you that they are auto-generated, and they
are not as eye-candy as the .pdf book.

### A Long Journey

This has been a long journey, and I have learned a lot while writing this book.
Mostly, I've learned how many people really needed such a book; and how this
whole book think is something than me.

And it was damn hard, you never know how tough it is when you actually dive
into it.

### What's Next

So what's next?

Since the book is complete; I'll have more time to focus on http://o2js.com/ .
I plan to write at least two articles per month. Some of those articles might
be complementary topics to the book, too ;).

### #JSIQ Version 2, Anyone?

I'm thinking about creating a "JavaScript Interview Questions v2 - Deluxe Edition",
feel free to share what you need for the next version of the book (
send and e-mail to volkan@o2js.com ). I'd love to hear your ideas, comments,
and suggestions.

### About the Author

My twitter handle is: @linkibol
And you can find more about me at http://volkan.io/ .

### Spread Out The Word

Without readers, a book is just a bundle of sheets.

You are the one who has made this e-book a "reality",
and I cannot thank you enough for that.

As always, I'd really appreciate if you spread the word by sharing this order
link ( http://o2js.com/interview-questions/ ) with people whom you think might
benefit from this book too.

Share http://o2js.com/interview-questions/ on twitter and Facebook;
Write a review on your blog; talk about it at lunch breaks…

I believe I should give thrice more than I get.
And I, to the best of my ability, try to add as much value to you as I can.

As always, feel free to ask questions about anything to me ( volkan@o2js.com ) –
I'll admit that I am generally terribly busy, and sometimes it takes weeks
(even months) to reply to e-mail; and I reply all my e-mails eventually.

And do whatever you can to spread the word!
If you find #JSIQ useful, why not help others too?


Respectfully,

Volkan Özçelik
