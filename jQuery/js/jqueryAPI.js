/**
 * Created by raynald on 12/6/13.
 */

(function() {

    var fName = "jQueryAPI() ";

    var core = {
        name: "core",
        "Core": ["jQuery()", "jQuery.holdReady()", "jQuery.noConflict()",
            "jQuery.sub()", "jQuery.when"]
    };

    var properties = {
      name: "properties",
      "Properties": [".context", ".jquery", "jQuery.browser",
          "jQuery.fx.interval", "jQuery.fx.off", "jQuery.support",
          ".length", ".selector"
      ]
    };
    
    var utilities = {
        name: "utilities",
        "Utilities": [ ".clearQueue", ".dequeue", "jQuery.boxModel",
            "jQuery.browser", "jQuery.contains()", "jQuery.data()",
            "jQuery.dequeue()", "jQuery.each()", "jQuery.extend()",
            "jQuery.fn.extend()", "jQuery.globalEval()", "jQuery.grep()",
            "jQuery.inArray()", "jQuery.isArray()", "jQuery.isEmptyObject()",
            "jQuery.isFunction()", "jQuery.isNumeric()",
            "jQuery.isPlainObject()", "jQuery.isWindow()", "jQuery.isXMLDoc",
            "jQuery.makeArray()", "jQuery.map()", "jQuery.merge()",
            "jQuery.noop()", "jQuery.now()", "jQuery.parseHTML()",
            "jQuery.parseJSON()", "jQuery.parseXML()", "jQuery.proxy()",
            "jQuery.queue()", "jQuery.removeData()", "jQuery.support",
            "jQuery.trim()", "jQuery.type()", "jQuery.unique()", ".queue()"
         ]
    };

    var selectors = {
        name: "selectors",
        "Attribute": [ "[name|='value']", "[name*='value']",
            "[name=~'value']", "[name$='value']", "[name='value']",
            "[name!='value']", "[name^='value']", "[name]",
            "[name='value'][name='value']" ],

        "Basic": [ "*", ".class",  "element", "#id" ],

        "Basic Filter": [ ":animated", ":eq()", ":even", ":first",
            ":focus", ":gt()", ":header", ":lang()", ":last", ":lt()",
            ":not()", ":odd", ":root", ":target" ],

        "Child Filter": [ ":first-child", ":first-of-type", ":last-child",
            ":last-of-type", ":nth-child()", ":nth-last-child()",
            ":nth-last-of-type()", ":nth-of-type()", ":only-child",
            ":only-of-type" ],

        "Content Filter": [ ":contains()", ":empty", ":has()", ":parent" ],

        "Form": [ ":button", ":checkbox", ":checked", ":disabled",
            ":enabled", ":file", ":focus", ":image", ":input",
            ":password", ":radio", ":reset", ":selected", ":submit",
            ":text" ],

        "Hierarchy": [ "parent > child", "ancestor descendant", "prev + next",
            "prev ~ siblings" ],

        "jQuery Extensions": [ ":animated", "[name!='value]", ":button",
            ":checkbox", ":eq()", ":even", ":file", ":gt", ":has()",
            ":header", ":hidden", ":image", ":input", ":last", ":lt()",
            ":odd", ":parent", ":password", ":radio", ":reset", ":selected",
            ":submit", ":text", ":visible" ],

        "Visibility": [ ":hidden", ":visible"]

    };
    
    var cssSelectors = {
        name: "css-selectors",
       "Attribute": [ "[name|='value']", "[name*='value']",
            "[name=~'value']", "[name$='value']", "[name='value']",
            "[name^='value']", "[name]"
         ],
         
        "Basic": [ "*", ".class",  "element", "#id" ],
        
        "Basic Filter": [ ":active", ":default", ":first",
            ":focus", ":hover", ":lang()", ":last", ":left", ":link",
            ":not()", ":root", ":target", ":visited" ],
            
        "Child Filter": [ ":first-child", ":first-of-type", ":last-child",
            ":last-of-type", ":nth-child()", ":nth-last-child()",
            ":nth-last-of-type()", ":nth-of-type()", ":only-child",
            ":only-of-type" ],                  
            
        "Content Filter": [ ":empty" ],         

        "Form": [ ":checked", ":disabled",
            ":enabled", ":focus", ":required", ":optional" ],
             
         "Hierarchy": [ "parent > child", "ancestor descendant", "prev + next",
            "prev ~ siblings" ],
            
         "Pseudo Elements": [ "::after", "::before", "::first-letter",
            "::first-line", "::selection" ]
    };

    var traversing = {
        name: "traversing",
        "Filtering": [ ".eq()", ".filter()", ".first()", ".has()", ".is()",
            ".last()", ".map()", ".not()", ".slice()" ],

        "Misc. Traversing": [ ".add()", ".addBack()", ".andSelf()",
            ".contents()", ".end()", ".not()" ],

        "Tree Traversal": [ ".children()", ".closest()", ".find()", ".next()",
            ".nextAll()", ".nextUntil()", ".offsetParent()", ".parent()",
            ".parents()", ".parentsUntil()", ".prev()", ".prevAll()",
            ".prevUntil()", ".siblings()" ]
    };

    var manipulation = {
        name: "manipulation",
        "Class Attribute": [".addClass()", ".hasClass()", ".removeClass()",
            ".toggleClass()" ],
        "Copying": [".clone()"],

        "DOM Insertion, Around": [ ".unwrap()", ".wrap()", ".wrapAll()",
            ".wrapInner()" ],

        "DOM Insertion, Inside": [ ".append()", ".appendTo()", ".html()",
            ".prepend()", ".prependTo()", ".text()"
        ],

        "DOM Insertion, Outside": [".after()", ".before()", ".insertAfter()",
            ".insertBefore()"
        ],

        "DOM Removal": [".detach()", ".empty()", ".remove()", ".unwrap()"],

        "DOM Replacement": [".replaceAll()", ".replaceWith()"],

        "General Attributes": [".attr()", ".prop", ".removeAttr()",
            ".removeProp()", ".val()"],

        "Style Properties": [".css()", ".height()", "innerHeight",
            ".innerWidth()", ".offset()", ".outerHeight()", ".outerWidth",
            ".position()", ".scrollLeft()", ".scrollTop()", ".width()"
        ]
    };

    var attributes = {
        name: "attributes",
        "Attributes":  [".addClass()", ".attr()", ".hasClass()", ".html()",
            ".prop()", ".removeAttr()", ".removeClass()", ".removeProp()",
            ".toggleClass()", ".val()"
         ]
    };

    var css = {
        name: "css",
        "CSS": [".addClass()", ".css()", ".hasClass()",".height()",
            ".innerHeight()", ".innerWidth()", "jQuery.cssHooks",
            ".offset()", ".outerHeight()", ".outerWidth()", ".position()",
            ".removeClass()", ".scrollLeft()", ".scrollTop()", ".toggleClass()",
            ".width()"
        ]
    };

    var dimensions = {
      name: "dimensions",
        "Dimensions": [".height()", ".innerHeight()", ".innerWidth()",
            ".outerHeight()", ".outerWidth()", ".width()"
        ]
    };
    
    var forms = {
      name: "forms",
        "Forms": [ ".blur()", ".change()", ".focus()", "jQuery.param",
            ".select()", ".serialize()", ".serializeArray()", ".submit()",
            ".val()"
        ]
    };
    
    var data = {
        name: "data",
            "Data": [ ".clearQueue()", ".data()", ".dequeue", "jQuery.data()",
                "jQuery.dequeue()", "jQuery.hasData()", "jQuery.queue",
                "jQuery.removeData()", ".queue()", ".removeData()"
        ]
    };
    
    var events = {
        name: "events",
        "Browser Events": [ ".error()", ".resize()", ".scroll()" ],
        "Document Loading": [ ".load()", ".ready()", ".unload()" ],
        "Handler Attachment": [ ".bind()", ".delegate()", ".die()",
            "jQuery.proxy", ".live()", ".off()", ".on()", ".one()", 
            ".trigger()", ".triggerHandler()", ".unbind()", ".undelegate()"
            ],
        
        "Event Object": [ ".currentTarget", ".data", 
            ".delegateTarget", ".isDefaultPrevented()",
            ".isImmediatePropagationStopped()", ".isPropagationStopped()",
            ".metaKey", ".namespace", ".pageX", ".pageY", ".preventDefault",
            ".relatedTarget", ".result", ".stopImmediatePropagation()",
            ".stopPropagation()", ".target", ".timeStamp", ".type", ".which"
            ],
         
         "Form Events": [ ".blur()", ".change()", ".focus()", ".focusin()",
            ".select()", ".submit()"
            ],              
        
          "Keyboard Events": [ ".focusout()", ".keydown()", ".keypress()",
            ".keyup()"
            ],
            
           "Mouse Events": [ ".click()", ".dblclick()", ".focusout()", ".hover",
                ".mousedown()", ".mouseenter()", ".mouseleave()", 
                ".mousemove()", ".mouseout()", ".mouseup()", ".toggle()"
            ]
    };
    
    var effects = {
        name: "effects",
        "Basics": [ ".hide()", ".show()", ".toggle()" ],
        "Custom": [ ".animate()", ".clearQueue()", ".delay()", ".dequeue()",
            ".finish()", ".jQuery.fx.interval", "jQuery.fx.off", ".queue()",
            ".stop()"
            ],
         "Fading": [ ".fadeIn()", ".fadeOut()", ".fadeTo()", ".fadeToggle()" ],
         "Sliding": [ ".slideDown()", ".slideToggle()", ".slideUp()" ]
    };
    
    var ajax = { 
        name: "ajax",
        "Global Event Handlers": [ ".ajaxComplete()", ".ajaxError()", 
            ".ajaxSend()", ".ajaxStart()", ".ajaxStop()", ".ajaxSuccess()"
         ],
         
         "Helper Functions": [ "jQuery.param()", ".serialize()", 
            ".serializeArray()"
         ],
         
         "Low Level Interface": [ "jQuery.ajax()", "jQuery.ajax.Prefilter()",
            "jQuery.ajaxSetup()", "jQuery.ajaxTransport()"
         ],
         
         "Shortcut Methods": [ "jQuery.get()", "jQuery.getJSON()",
            "jQuery.getScript()", "jQuery.post()", ".load()"
          ]
    };
    
    var miscellaneous = {
        name: "miscellaneous",
        "Collection Manipulation": [ ".each()", "jQuery.param()" ],
        
        "Data Storage": [ ".data()", ".removeData()" ],
        
        "DOM Element Methods": [ ".get()", ".index()", ".size()",
            ".toArray()"
         ],
         
        "Setup Methods": [ "jQuery.noConflict()" ]
    };
    
    var deferred = {
        name: "deferred",
        "Deferred": [ ".always()", ".done()", ".fail()", ".isRejected()",
            ".isResolved()", ".notify()", ".notifyWith()", ".pipe()",
            ".progress()", ".promise()", ".reject()", ".rejectWith()",
            ".resolve()", ".resolveWith()", ".state()", ".then()",
            "jQuery.Deferred", "jQuery.when()"
         ]
    };
    
    var callbacks = {
        name: "callbacks",
        "Callbacks": [ "callbacks.add()", "callbacks.disable()",
            "callbacks.disabled()", "callbacks.empty()", "callbacks.fire()",
            "callbacks.fired()", "callbacks.fireWith()", "callbacks.has()",
            "callbacks.lock()", "callbacks.locked()", "callbacks.remove()",
            "jQuery.Callbacks()"
         ]
     };
     
     var internals = {
        name: "internals",
        "Internals": [ ".context", ".jquery", "jQuery.error()", ".pushStack()",
            ".selector"
         ]
     };
    
    // table = DOM <table> element
    // obj = object containing array of API names
    function fillTables(table, obj ) {
        var fName = 'fillTables(): ';

        // add new row to hold column data
        var sel = '#' + table.id + ' tbody';

        console.info(fName + 'sel = ' + sel);

        var $tr = $(sel).append("<tr></tr>");

        $.each(obj, function(k, v) {
            // k is is object property
            // v is the object value (the array)
            // console.info(fName + 'key = ' + k);

            if (k !== 'name') {
                var str = '<ul>';

                // try out jQuery each() function !
                $.each(v, function(i, val) {
                    str += "<li><a href='#'>" + val + "</a></li>";
                });

                str += '</ul>';

                $("<td></td>").html(str).appendTo($tr);
            }

        });
    }

    var i;
    var tableArr = [];
    tableArr.push(core);
    tableArr.push(properties);
    tableArr.push(selectors);
    tableArr.push(cssSelectors);
    tableArr.push(traversing);
    tableArr.push(manipulation);
    tableArr.push(attributes);
    tableArr.push(css);
    tableArr.push(dimensions);
    tableArr.push(forms);
    tableArr.push(data);
    tableArr.push(events);
    tableArr.push(effects);
    tableArr.push(ajax);
    tableArr.push(miscellaneous);
    tableArr.push(deferred);
    tableArr.push(callbacks);
    tableArr.push(utilities);
    tableArr.push(internals);
    
    // need to save the size of array of objects holding API names
    var len = tableArr.length;

    // number of <table class='display'>... DOM elements
    var tables = $("table.display").get();

    var numOfTables = tables.length;
    console.info(fName + "num of tables: " + numOfTables);

    for (i = 0; i < numOfTables; i++) {
        console.info(fName + "<table id='" + tables[i].id + "'>");
    }

    if (1) {
        $.each(tables, function(idx, table) {
            var obj = tableArr.shift();
            console.info (fName + idx + ": name: " + obj["name"]);
            fillTables(table, obj);
        });
    } else {
        // save the length 'cause each time we call tableArr.shift()
        // array length will change!
        for (i = 0; i < numOfTables; i++ ) {
            var obj = tableArr.shift();
            console.info (fName + i + ": name: " + obj["name"]);
            fillTables(tables[i], obj);
        }
    }

})();
