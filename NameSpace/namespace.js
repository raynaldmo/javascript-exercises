/**
 * Created by raynald on 3/21/14.
 */
var M;

$(function(){
  console.log("namespace loaded");
  var testString = "I am a test string";
  console.log(testString);

  // does this variable persist across page loads/re-loads ?
  // No, its re-init.
  var N = N || [];
  N.push([1,2,3]);
  console.log('N ->',N.toString());

  // same here for global
  M = M || [];
  M.push([1,2,3]);
  console.log('M ->',M.toString());
});
