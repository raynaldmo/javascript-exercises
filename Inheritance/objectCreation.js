// The two most important principles in OOP are object creation patterns
// (ENCAPSULATION) and code re-use patterns (INHERITANCE)

// various ways to create objects

//****************** OBJECT CREATION PATTERNS ***************//
// 1. One by one (tedious to repeat, a lot of code duplication

var person new Object();
person.name = "Raynald";
person.age = 53;
person.job = "Software Engineer";
person.sayName = function() {
    alert(this.name);
};

// use object literal
var person = {
    name: "Raynald",
    age: 53,
    job: "Software Engineer",
    sayName : function() {
        alert(this.name);
    }
};

// 2. Factory Pattern (doesn't support object identification, 
// all objects are Object type)

function createPerson(name, age, job) {
    var o = new Object();
    o.name = name;
    o.age = age;
    o.job = job;
    o.sayName = function () {
        alert(this.name);
    };
    return o;
}

var person1 = createPerson("Raynald", 53, "Software Engineer");

// 3. Constructor Pattern
// (supports object identification)

// function F() {}
// instance1 = new F()
// instance2 new F()
// Values and methods assigned to to F.prototype will be available
// on all instances of F, instance1, instance2, ...

// instance1.constructor === F  (true)
// instance2.constructor === F (true)

// F.prototype.constructor === F (true)

function Person(name, age, job) {
    this.name = name;
    this.age = age;
    this.job = job;

    // drawback to this pattern is that each instance has
    // its own separate copy of methods (functions)
    // instance1 and instance2 will have a separate copy of method
    // sayName().
    // this is wasteful code duplication
    this.sayName = function () {
        alert(this.name);
    };
}

var person1 = new Person("Raynald", 53, "Software Engineer");

person1.constructor == Person // true
person1 instanceof Person // true


// 4. Constructor/Prototype Pattern ******
// (Most widely used/best practice for creating custom object types)
// Provides encapsulation
// Encapsulation encloses all the functionality  of an object
// within the object itself
function Person(name, age, job) {
    this.name = name;
    this.age = age;
    this.job = job;
    this.friends = ["Mike", "Juke"];
}

// object literal defining properties to inherit
// instances will share the function sayName()
Person.prototype = {
    constructor: Person,
    sayName: function() {
        alert(this.name);
    }
};

var person1 = new Person("Raynald", 53, "Software Engineer");
var person2 = new Person("Blaqson", 44, "Player");

person1.friends.push("Smokey");

// person1 and person2 have separate reference properties
// Below shows person1.friends array was modified but not person2.friends
// array
person1.friends // "Mike, Juke, Smokey"
person2.friends // "Mike, Juke"



// ******************* INHERITANCE MECHANISMS ***************************//
// CODE RE-USE

// 1. Prototype Chaining

// constructor
function SuperType() {
    this.property = true;
}

// add method to object type SuperType
SuperType.prototype.getSuperValue = function() {
    return this.property;
};

// constructor
function SubType() {
    this.subproperty = false;
}

// inherit from SuperType
// creates new object of type SuperType and assign
// to SubType type prototype (that's a mouthful!)
SubType.prototype = new SuperType();

// add method to object type SubType
SubType.prototype.getSubValue = function () {
    return this.subproperty;
};

var instance = new SubType();

instance.getSuperValue(); // true;

//----------------------PROTOTYPE CHAIN --------------------------
// object instances inherit properties from its prototype.
// Because the prototype is also an object, it has its own prototype
// and inherits properties from that.
// This is the prototype chain:
// An object inherits from its prototype, while that prototype in turn
// inherits from its prototype, and so on.

// How does instantiated object find its properties ?
// a. Look for the property in the object itself
// b. Every object instance has a constructor property that points
// to constructor that created it
// All constructors have a prototype property
// c. Look for the property in:
// instance.constructor.prototype -> inherited object -> constructor -> prototype



// 1a. Problems with Prototype Chaining
// (change in inherited reference value propagates to all instances)

function SuperType() {
    this.colors = ["red", "blue", "green"];
}

function SubType() {
}

// inherit from SuperType
SubType.prototype = new SuperType();

var instance1 = new SubType();
instance1.colors.push("black");

alert(instance.colors) // "red, blue, green, black"

var instance2 = new SubType();

alert(instance2.colors); // "red, blue, green, black"


// 2. Constructor Stealing (Inheritance)
// Ensures that class instances have their own copy of
// reference (arrays, and objects) values defined in constructor

// If used alone, without prototype chaining, constructor stealing
// leads to duplicate function methods in each instance.
// Solution is to use Combination Inheritance - see below

function SuperType() {
    this.colors = ["red", "blue", "green"];
}

function SubType() {
    // inherit from SuperType
    // This below is an interesting expression
    // it's equivalent to: this.SuperType()
    // where __this__ is the object created by the call new SubType()
    // (see below)
    // This then adds the colors array to obj
    SuperType.call(this);
}

var instance1 = new SubType();
instance1.colors.push("black");
console.info(instance1.colors); // "red, blue, green, black"

var instance1 = new SubType();
console.info(instance2.colors); // "red, blue, green"

// 3. Combination Inheritance 
// (Most frequently used inheritance pattern)

function SuperType(name) {
    this.name = name;
    // each instance has its own copy of the colors array
    this.colors = ["red", "blue", "green"];
} 

// one copy of sayName() for all instances
SuperType.prototype.sayName = function() {
    alert(this.name);
};

function SubType(name, age) {
    // "constructor stealing"
    SuperType.call(this, name);
    this.age = age;
}

// prototype chaining
// inherit methods from SuperType object type. values (including)
// reference values are not inherited
SubType.prototype = new SuperType();

SubType.prototype.sayAge = function() {
    alert(this.age);
};

var instance1 = new SubType("Nicholas", 29);
instance1.colors.push("black");
alert(instance1.colors);  //"red,blue,green,black"
instance1.sayName();      //"Nicholas";
instance1.sayAge();       //29
                   
var instance2 = new SubType("Greg", 27);
alert(instance2.colors);  //"red,blue,green"
instance2.sayName();      //"Greg";
instance2.sayAge();       //27


// 4. Prototypal Inheritance
// (Advantage is that there is no need to create custom object
// types


function object(o){
    function F(){}
    // !! this is the key line - the inheritance mechanism works
    // by manipulating/setting the constructor's prototype property.
    // in this case F is the constructor
    F.prototype = o;
    return new F();
}

// The object() function creates a temporary constructor, assigns a given 
// object as the constructor’s prototype, and returns a new instance of the 
// temporary type. Essentially, object() performs a shadow copy of any object 
// that is passed into it. Consider the following:

var person = {
    name: "Nicholas",
    friends: ["Shelby", "Court", "Van"]
};
                   
var anotherPerson = object(person);
anotherPerson.name = "Greg";
anotherPerson.friends.push("Rob");
                   
var yetAnotherPerson = object(person);
yetAnotherPerson.name = "Linda";
yetAnotherPerson.friends.push("Barbie");
                   
alert(person.friends);   //"Shelby,Court,Van,Rob,Barbie"

// 5. Parasitic Combination Inheritance
// (RECCOMMENDED!!!!!!!!

function object(o) {
    function F(){}
    F.prototype = o;
    return new F();
}

function inheritPrototype(subType, superType) {
    var prototype = object(superType.prototype);
    prototype.constructor = subType;
    subType.prototype = prototype;
}


function SuperType(name) {
    this.name = name;
    this.colors = ["red", "blue", "green" ];
}

SuperType.prototype.sayName = function() {
    alert(this.name);
};

function SubType(name, age) {
    SuperType.call(this, name);
    this.age = age;
}

inheritPrototype(SubType, SuperType);

// Alternate way to using inheritProtoType
SubType.prototype = Object.create(SuperType.prototype,
  { constructor: {
      configurable: true,
      enumerable : true,
      value: SubType,
      writable: true
    }
  }
);

SubType.prototype.sayAge = function() {
    alert(this.age);
};


